# cyrating-gandi

A python wrapper for [GANDI API (v5)](https://api.gandi.net) to easily manage defensive domains.
This wrapper works with your domains using GANDI's LiveDNS service only. In addition, there are some [limitations](#limits).

## Usage

Authentication to Gandi's API require a Gandi API key. If you don’t have one yet, you can generate your production API key from the [API Key Page](https://account.gandi.net/) (in the Security section).

To use this API key, fill the configuration file "gandi.ini" with the following:

```sh
$ echo -e "[gandi]\napi_key = gandi_apikey" > gandi.ini
```

**List organizations**
```sh
$ gandi-cli.py --list-organizations
[...]
```

**List domains**
```sh
$ gandi-cli.py --list-domains
[...]
```
For domains belonging to other organization, you need to specify the sharing_id (id of the organization):
```sh
$ gandi-cli.py --list-domains --sharing-id XXXXX-XXX-XXX-XXXXX
[...]
```

**Set defensive values to domain**

```sh
$ gandi-cli.py --sharing-id XXXXX-XXX-XXX-XXXXX --set-defensive-values domain.tld
[...]
```

**Enable Gandi's LiveDNS service**

```sh
$ gandi-cli.py --sharing-id XXXXX-XXX-XXX-XXXXX --enable-livedns domain.tld
[...]
```

**Other options are available through help**
```sh
$ gandi-cli.py --help
[...]
```

## Limits

Gandi's API v5 is still in beta and all configuration settings are not available through this API. Thus, the following options is not available yet:

 - Enabling DNSSEC (We understand from Gandi that this feature will be available prior july 2020 - to be confirmed)
 - Registry locks

That's why you should configure them manually via the Gandi's web application.

## Meta

Cyrating – [@cyrating](https://twitter.com/cyrating) – hello@cyrating.com

Distributed under the MIT license. See ``LICENSE`` for more information.


## Contributing

1. Send issues to issues@cyrating.com

