#!/usr/bin/env python3.6

import sys
import os
import argparse

from cyrating_gandi import Gandi

__progname__ = os.path.basename(sys.argv[0])
__author__ = "Cyrating"
__version__ = "0.1.0"
__license__ = "MIT"


def usage():
  print("""Usage: %s [-h] domain
  -h, --help        print this usage and exit"""
        % (__progname__))
  sys.exit(1)


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Wrapper to GANDI\'s API v5')
  parser.add_argument('--list-organizations', dest='list_organizations', action="store_true", default=False, help="List organizations",)
  parser.add_argument('--list-domains', dest='list_domains', action="store_true", default=False, help="List domains",)
  parser.add_argument('--sharing-id', dest='sharing_id', action="store", default=None, help="Specify the organization ID for domain belonging to other organization", metavar='SHARING_ID')

  parser.add_argument('--domain-info', dest='domain_info', action="store", default=None, help="Print information about a domain", metavar='DOMAIN')
  parser.add_argument('--enable-livedns', dest='enable_livedns', action="store", default=None, help="Enable Gandi's LiveDNS service", metavar='DOMAIN')

# parser.add_argument('--enable-dnssec', dest='enable_dnssec', action="store", default=None, help="Enable DNSSEC for a domain", metavar='DOMAIN')
  parser.add_argument('--list-dnssec-keys', dest='list_dnssec_keys', action="store", default=None, help="List DNSSEC keys of a domain", metavar='DOMAIN')
  parser.add_argument('--delete-dnssec-keys', dest='delete_dnssec_keys', action="store", default=None, help="Delete all DNSSEC keys of a domain", metavar='DOMAIN')

  parser.add_argument('--list-records', dest='list_records', action="store", default=None, help="List records of a domain", metavar='DOMAIN')
  parser.add_argument('--delete-records', dest='delete_records', action="store", default=None, help="Delete all records of a domain", metavar='DOMAIN')
  parser.add_argument('--set-defensive-zone', dest='set_defensive_zone', action="store", default=None, help="Create records with defensive values for a domain", metavar='DOMAIN')

  parser.add_argument('--create-snapshot', dest='create_snapshot', action="store", default=None, help="Create a snapshot of records for a domain", metavar='DOMAIN')

  parser.add_argument('--set-defensive-values', dest='set_defensive_values', action="store", default=None, help="Set default configuration for a defensive domain (default DNS zone and enable registrar transfer lock)", metavar='DOMAIN')

  parser.add_argument('--enable-registrar-transfer-lock', dest='enable_registrar_transfer_lock', action="store", default=None, help="Enable transfer lock at registrar level", metavar='DOMAIN')
  parser.add_argument('--disable-registrar-transfer-lock', dest='disable_registrar_transfer_lock', action="store", default=None, help="Disable transfer lock at registrar level", metavar='DOMAIN')

  args = parser.parse_args()

  gandi = Gandi()

  if args.list_organizations is True:
    gandi.list_organizations()

  if args.list_domains is True:
    gandi.list_domains(args.sharing_id)

  if args.domain_info is not None:
    gandi.domain_info(args.domain_info, args.sharing_id)

  if args.list_dnssec_keys is not None:
    gandi.list_dnssec_keys(args.list_dnssec_keys, args.sharing_id)

# if args.enable_dnssec is not None:
#   gandi.create_dnssec_key_ksk(args.enable_dnssec, args.sharing_id)

  if args.list_records is not None:
    gandi.list_domain_records(args.list_records, args.sharing_id)

  if args.create_snapshot is not None:
    gandi.create_snapshot(args.create_snapshot, args.sharing_id)

  if args.delete_records is not None:
    gandi.delete_records(args.delete_records, args.sharing_id)

  if args.set_defensive_zone is not None:
    domain = args.set_defensive_zone
    gandi.create_default_records(args.set_defensive_zone, args.sharing_id)

  if args.delete_dnssec_keys is not None:
    gandi.delete_dnssec_keys(args.delete_dnssec_keys, args.sharing_id)

  if args.set_defensive_values is not None:
    domain = args.set_defensive_values
    gandi.create_default_records(domain, args.sharing_id)
    gandi.enable_registrar_transfer_lock_(domain, args.sharing_id)
    # gandi.create_dnssec_key_ksk(domain, args.sharing_id)

  if args.enable_livedns is not None:
    gandi.enable_livedns(args.enable_livedns, args.sharing_id)

  if args.enable_registrar_transfer_lock:
    gandi.enable_registrar_transfer_lock_(args.enable_registrar_transfer_lock, args.sharing_id)

  if args.disable_registrar_transfer_lock:
    gandi.disable_registrar_transfer_lock_(args.disable_registrar_transfer_lock, args.sharing_id)
