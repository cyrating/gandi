defaults_zone = """@ 10800 IN MX 0 .
@ 10800 IN CAA 0 issue ";"
@ 10800 IN CAA 0 issuewild ";"
@ 10800 IN TXT "v=spf1 -all"
_dmarc 10800 IN TXT "v=DMARC1; p=reject; pct=100;adkim=s; aspf=s;"
"""
