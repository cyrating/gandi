from cyrating_gandi.defaults import defaults_zone
import configparser
import requests
import json
import pprint

pp = pprint.PrettyPrinter(indent=2)


class Gandi(object):
  def __init__(self, **kwargs):
    self._api_url = 'https://api.gandi.net/v5'
    self._config_path = 'gandi.ini'
    self._default_path = 'defaults.ini'

    # Read settings
    config = configparser.ConfigParser()
    config.read(self._config_path)
    self._api_key = config['gandi']['api_key']

  @staticmethod
  def get_default_record(config, name):
    return dict(
        rrset_name=config[name]['rrset_name'],
        rrset_ttl=int(config[name]['rrset_ttl']),
        rrset_type=config[name]['rrset_type'],
        rrset_values=eval(config[name]['rrset_values'])
    )

  def make_params(self, sharing_id):
    if sharing_id is None:
      return None
    return {'sharing_id': sharing_id}

  def get(self, endpoint, sharing_id=None):
    headers = {
        "Content-Type": "application/json",
        "Authorization": 'Apikey ' + self._api_key
    }
    params = self.make_params(sharing_id)

    url = self._api_url + '/' + endpoint
    try:
      res = requests.get(url, headers=headers, params=params)
      if res.ok:
        return json.loads(res.content)
    except Exception:
      return None
    return None

  def post(self, endpoint, data, sharing_id=None):
    headers = {'Authorization': 'Apikey ' + self._api_key}

    if data is not None:
      headers['Content-Type'] = 'application/json'

    params = self.make_params(sharing_id)

    url = self._api_url + '/' + endpoint
    try:
      res = requests.post(url, headers=headers, params=params, data=json.dumps(data) if data else None)
      if res.ok:
        return json.loads(res.content)
    except Exception:
      return None
    return None

  def put(self, endpoint, data, sharing_id=None):
    headers = {
        "Content-Type": "text/plain",
        "Authorization": 'Apikey ' + self._api_key
    }
    params = self.make_params(sharing_id)

    url = self._api_url + '/' + endpoint
    try:
      res = requests.put(url, headers=headers, params=params, data=data)
      if res.ok:
        return True
    except Exception:
      return False
    return False

  def patch(self, endpoint, data, sharing_id=None):
    headers = {
        "Content-Type": "application/json",
        "Authorization": 'Apikey ' + self._api_key
    }
    params = self.make_params(sharing_id)

    url = self._api_url + '/' + endpoint
    try:
      res = requests.patch(url, headers=headers, params=params, data=data)
      print(res.text)
      if res.ok:
        return True
    except Exception:
      return False
    return False

  def delete(self, endpoint, sharing_id=None):
    headers = {
        # "Content-Type": "application/json",
        "Authorization": 'Apikey ' + self._api_key
    }
    params = self.make_params(sharing_id)

    url = self._api_url + '/' + endpoint
    try:
      res = requests.delete(url, headers=headers, params=params)
      print('status-code', res.status_code)
      print(res.content)
      if res.ok:
        return None
    except Exception:
      return None
    return None

  def list_domains(self, sharing_id=None):
    domains = self.get('livedns/domains', sharing_id)

    if domains is None:
      print('No domain has been identified')
      return

    pp.pprint(domains)

  def list_organizations(self):
    orgs = self.get('organization/organizations')

    if orgs is None:
      print('No organization has been identified')
      return

    pp.pprint(orgs)

  def domain_info(self, domain, sharing_id=None):
    info = self.get('livedns/domains/' + domain, sharing_id)
    pp.pprint(info)

  def list_dnssec_keys(self, domain, sharing_id=None):
    dnssec_keys = self.get('livedns/domains/' + domain + '/keys', sharing_id)

    if dnssec_keys is None:
      print('No dnssec key has been found')
      return

    pp.pprint(dnssec_keys)
    return dnssec_keys

  def create_dnssec_key(self, domain, type, sharing_id=None):
    dnssec_keys = self.post('livedns/domains/' + domain + '/keys', data={'flags': 256 if type == 'ZSK' else 257}, sharing_id=sharing_id)

    if dnssec_keys is None:
      print('Error to create DNSSEC key')
      return

    pp.pprint(dnssec_keys)

  def create_dnssec_key_zsk(self, domain, sharing_id=None):
    return self.create_dnssec_key(domain, 'ZSK', sharing_id)

  def create_dnssec_key_ksk(self, domain, sharing_id=None):
    return self.create_dnssec_key(domain, 'KSK', sharing_id)

  def list_domain_records(self, domain, sharing_id=None):
    records = self.get('livedns/domains/' + domain + '/records', sharing_id=sharing_id)

    if records is None:
      print('No record has been identified')
      return

    pp.pprint(records)

  def create_snapshot(self, domain, sharing_id=None):
    snapshot = self.post('livedns/domains/' + domain + '/snapshots', data={}, sharing_id=sharing_id)

    if snapshot is None:
      print('Error to create snapshot')
      return

    pp.pprint(snapshot)

  def delete_records(self, domain, sharing_id=None):
    self.delete('livedns/domains/' + domain + '/records', sharing_id=sharing_id)

  def create_record(self, domain, record, sharing_id=None):
    r = self.post('livedns/domains/' + domain + '/records', data=record, sharing_id=sharing_id)
    pp.pprint(r)

  def create_default_records(self, domain, sharing_id=None):
    res = self.put('livedns/domains/' + domain + '/records', defaults_zone, sharing_id)
    if res is False:
      print('Error to update zone file')

  def delete_dnssec_keys(self, domain, sharing_id=None):
    keys = self.list_dnssec_keys(domain, sharing_id)
    for key in keys:
      self.delete('livedns/domains/' + domain + '/keys/' + key['id'], sharing_id)

  def enable_livedns(self, domain, sharing_id=None):
    r = self.post('domain/domains/' + domain + '/livedns', data=None, sharing_id=sharing_id)
    pp.pprint(r)

  def enable_registrar_transfer_lock_(self, domain, sharing_id=None):
    self.patch('domain/domains/' + domain + '/status', data="{\"clientTransferProhibited\":true}", sharing_id=sharing_id)

  def disable_registrar_transfer_lock_(self, domain, sharing_id=None):
    self.patch('domain/domains/' + domain + '/status', data="{\"clientTransferProhibited\":false}", sharing_id=sharing_id)

